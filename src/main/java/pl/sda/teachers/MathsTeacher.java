package pl.sda.teachers;

import pl.sda.motto.SadMottoService;

public class MathsTeacher implements Teacher {

    private final SadMottoService sadMottoService;

    public MathsTeacher(SadMottoService sadMottoService) {
        this.sadMottoService = sadMottoService;
    }

    public String greet(){
        return "Hi, I'm the Maths teacher.";
    }

    public String sayMotto() {
        return sadMottoService.getMotto();
    }

    private String emailAddress;

    public String getEmailAddress() {
        return emailAddress;
    }

    public MathsTeacher setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }
}
