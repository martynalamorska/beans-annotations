package pl.sda.teachers;

public interface Teacher {
    public String greet();

    public String sayMotto();
}
