package pl.sda.appConfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.sda.motto.HappyMottoService;
import pl.sda.motto.SadMottoService;
import pl.sda.teachers.EnglishTeacher;
import pl.sda.teachers.MathsTeacher;
import pl.sda.teachers.PhysicsTeacher;

@Configuration
public class ApplicationConfig {

    @Bean
    HappyMottoService happyMottoService(){
        return new HappyMottoService();
    }


    @Bean
    SadMottoService sadMottoService(){
        return new SadMottoService();
    }

    @Bean
    MathsTeacher mathsTeacher(){
        return new MathsTeacher(sadMottoService());
    }

    @Bean
    PhysicsTeacher physicsTeacher(){
        return new PhysicsTeacher(happyMottoService());
    }

    @Bean
    EnglishTeacher englishTeacher(){
        return new EnglishTeacher();
    }
}
